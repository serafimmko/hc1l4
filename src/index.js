import React from 'react'
import { render } from 'react-dom'
import HeaderMenu from './components/HeaderMenu'
import Main from './components/Main'
import Footer from './components/Footer'
import Catalog from './pages/Catalog'
import ContactUs from './pages/ContactUs'

const App = () =>
  <div>
    <HeaderMenu/>
    <Main/>
    <Footer/>
  </div>

render(<Catalog/>, document.getElementById('root'))
