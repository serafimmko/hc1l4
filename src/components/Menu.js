import React from 'react'

const Menu = () =>
  <ul>
    <li><a href="index.html">Главная</a></li>
    <li><a href="catalog.html">Каталог</a></li>
    <li><a href="contacts.html">Контакты</a></li>
  </ul>

export default Menu
