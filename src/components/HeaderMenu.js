import React from 'react'
import Menu from './Menu'
import MyImg from '../../public/images/i.jpg'

const HeaderMenu = () =>
  <header>
    <img src={MyImg} alt='logo'/>
    <Menu/>
  </header>

export default HeaderMenu