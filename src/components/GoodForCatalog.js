import React from 'react'
// todo: here i can take my image, but i need take into the component
import myImage from '../../public/images/1.jpg'

const GoodForCatalog = ({ shortDesc, name, image }) => {
  return (
    <div>
      <img src={myImage} alt="Товар 1" title="Товар 1" width="200" height="200"/>
      <p className="short-description">
        {shortDesc}
      </p>
      // todo: i need create handle for this, or another
      <a href="products/1.html">{name}</a>
    </div>
  )
}

export default GoodForCatalog